#include <sstream>
#include <utility>
#include <vector>

#include "rectangles.h"

namespace rectangles {

    std::string Rectangle::toString() const {
       std::ostringstream oss;
       oss << "(" << x << ", " << y << "), w=" << w << ", h=" << h;
       return oss.str();
    }

    bool Rectangle::operator==(const Rectangle& rhs) const noexcept {
        return this->x == rhs.x && this->y == rhs.y && this->w == rhs.w && this->h == rhs.h;
    }

    bool Rectangle::operator!=(const Rectangle& rhs) const noexcept {
        return !(*this == rhs);
    }

    Rectangle Rectangle::intersect(const Rectangle& rect) const noexcept {
       std::pair<int, int> topLeft = std::make_pair(
           std::max(x, rect.x),
           std::max(y, rect.y));

       std::pair<int, int> bottomRight = std::make_pair(
           std::min(x + w, rect.x + rect.w),
           std::min(y + h, rect.y + rect.h));

       return {
           topLeft.first,
           topLeft.second,
           std::max(0, bottomRight.first - topLeft.first),
           std::max(0, bottomRight.second - topLeft.second) };
    }

    std::vector<Intersection> computeIntersections(const std::vector<Rectangle>& rects) {
       std::vector<Intersection> intersections;

       for (int i = 0; i < (int) rects.size(); ++i) {
           int size = intersections.size();
           for (int j = 0; j < size; ++j) {
               Rectangle rect = intersections[j].second.intersect(rects[i]);

               std::vector<int> ids = intersections[j].first;
               ids.push_back(i);

               if (!rect.empty()) {
                   intersections.push_back({std::move(ids), rect});
               }
           }

           intersections.push_back({{i}, rects[i]});
        }

        return intersections;
    }

}
