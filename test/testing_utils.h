#ifndef TESTING_UTILS_H
#define TESTING_UTILS_H

#include <iostream>
#include <map>
#include <stdexcept>

struct AssertionError: public std::exception {
    virtual const char* what() const noexcept {
        return "assertion error";
    }

    virtual ~AssertionError() {}
};

template<typename T>
void assertEquals(T first, T second) {
    if (first != second) {
        throw AssertionError();
    }
}

void assertTrue(bool statement) {
    if (!statement) {
        throw AssertionError();
    }
}

void assertFalse(bool statement) {
    if (statement) {
        throw AssertionError();
    }
}

using Testcase = bool (*)();

void runTest(const std::map<std::string, Testcase>& testcases) {
    for (const auto& entry: testcases) {
        if (entry.second()) {
            std::cout << entry.first << ": PASSED\n";
        } else {
            std::cout << entry.first << ": FAILED\n";
        }
    }
}

#endif
