#include <algorithm>
#include <map>
#include <vector>

#include "../rectangles.h"
#include "testing_utils.h"

bool failToCreateRectangleWithNegativeWidth() {
    try {
        rectangles::Rectangle rect(0, 0, -100, 100);
    } catch(std::invalid_argument& e) {
        return true;
    }
    return false;
}

bool failToCreateRectangleWithNegativeHeight() {
    try {
        rectangles::Rectangle rect(0, 0, 500, -500);
    } catch(std::invalid_argument& e) {
        return true;
    }
    return false;
}

bool failToCreateRectangleDueToWidthOverflow() {
    try {
        rectangles::Rectangle rect(2000000000, 0, 2000000000, 0);
    } catch(std::invalid_argument& e) {
        return true;
    }
    return false;
}

bool failToCreateRectangleDueToHeightOverflow() {
    try {
        rectangles::Rectangle rect(0, 2000000000, 0, 2000000000);
    } catch(std::invalid_argument& e) {
        return true;
    }
    return false;
}

bool createRectanglesOfDifferentSizes() {
    try {
        rectangles::Rectangle rect1(1000000000, 1000000000, 1000000000, 1000000000);
        rectangles::Rectangle rect2(2000000000, -1000000000, 0, 2000000000);
        rectangles::Rectangle rect3(-2000000000, -2000000000, 2000000000, 2000000000);
        rectangles::Rectangle rect4(-2000000000, -2000000000, 0, 0);
        rectangles::Rectangle rect5(0, 0, 2000000000, 2000000000);
    } catch(std::invalid_argument& e) {
        return false;
    }
    return true;
}

bool checkIfRectangleIsEmpty() {
    rectangles::Rectangle rect1(100, 100, 0, 0);
    rectangles::Rectangle rect2(200, -100, 0, 100);
    rectangles::Rectangle rect3(-200, -200, 200, 0);
    rectangles::Rectangle rect4(-200, -200, 1, 1);
    rectangles::Rectangle rect5(0, 0, 200, 200);

    try {
        assertTrue(rect1.empty());
        assertTrue(rect2.empty());
        assertTrue(rect3.empty());
        assertFalse(rect4.empty());
        assertFalse(rect4.empty());
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

std::vector<rectangles::Rectangle> first = {
    { 100, 100, 400, 100 },
    { 0, 0, 400, 400 },
    { -100, -200, 400, 200 },
    { 0, 0, 100, 100 },
    { 1000, 100, 200, 300 },
    { -500, -500, 100, 200 },
    { 0, 0, 200, 100 },
    { -7000, -8000, 250, 150 },
    { 100, 100, 100, 100 },
    { 100, 100, 50, 50 }
};

std::vector<rectangles::Rectangle> second = {
    { 200, 0, 100, 400 },
    { 100, 100, 100, 100 },
    { 0, -100, 100, 200 },
    { 50, -50, 100, 100 },
    { 1000, 200, 100, 100 },
    { -500, -400, 100, 100 },
    { 100, 0, 100, 200 },
    { 0, 0, 2500, 1500 },
    { 200, 100, 100, 100 },
    { 150, 150, 50, 50 }
};

bool intersectionReflexive() {
    try {
        for (const auto& rect: first) {
            assertEquals(rect.intersect(rect), rect);
        }
        for (const auto& rect: second) {
            assertEquals(rect.intersect(rect), rect);
        }
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

bool intersectionSymmetric() {
    try {
        for (unsigned i = 0; i < first.size(); ++i) {
            assertEquals(first[i].intersect(second[i]), second[i].intersect(first[i]));
        }
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

bool intersectionResults() {
    try {
        assertEquals(first[0].intersect(second[0]), { 200, 100, 100, 100 });
        assertEquals(first[1].intersect(second[1]), { 100, 100, 100, 100 });
        assertEquals(first[2].intersect(second[2]), { 0, -100, 100, 100 });
        assertEquals(first[3].intersect(second[3]), { 50, 0, 50, 50 });
        assertEquals(first[4].intersect(second[4]), { 1000, 200, 100, 100 });
        assertEquals(first[5].intersect(second[5]), { -500, -400, 100, 100 });
        assertEquals(first[6].intersect(second[6]), { 100, 0, 100, 100 });

        assertTrue(first[7].intersect(second[7]).empty());
        assertTrue(first[8].intersect(second[8]).empty());
        assertTrue(first[9].intersect(second[9]).empty() );
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

bool computeIntersectionsEmptyList() {
    auto intersections = rectangles::computeIntersections(std::vector<rectangles::Rectangle>());

    try {
        assertTrue(intersections.empty());
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

std::map<std::vector<int>, rectangles::Rectangle> intersToMap(
        const std::vector<rectangles::Intersection>& inters) {
    std::map<std::vector<int>, rectangles::Rectangle> intersMap;

    for (const auto& inter: inters) {
        intersMap[inter.first] = inter.second;
    }

    return intersMap;
}

bool computeIntersectionsTheSameRect() {
        std::vector<rectangles::Rectangle> rectangles;
        for (int i = 0; i < 3; ++i) {
            rectangles.push_back({ 0, 0, 100, 100 });
        }
        auto inters = intersToMap(rectangles::computeIntersections(rectangles));

    try {
        assertEquals((int) inters.size(), 7);

        assertTrue(inters.find({ 0 }) != inters.end());
        assertTrue(inters.find({ 1 }) != inters.end());
        assertTrue(inters.find({ 2 }) != inters.end());
        assertTrue(inters.find({ 0, 1 }) != inters.end());
        assertTrue(inters.find({ 0, 2 }) != inters.end());
        assertTrue(inters.find({ 1, 2 }) != inters.end());
        assertTrue(inters.find({ 0, 1, 2 }) != inters.end());

        for (const auto& intersection: inters) {
            assertEquals(intersection.second, { 0, 0, 100, 100 });
        }
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

bool computeIntersectionsDifferentRects1() {
            std::vector<rectangles::Rectangle> rectangles;
            rectangles.push_back({ 100, 100, 250, 80 });
            rectangles.push_back({ 120, 200, 250, 150 });
            rectangles.push_back({ 140, 160, 250, 100 });
            rectangles.push_back({ 160, 140, 350, 190 });
            auto inters = intersToMap(rectangles::computeIntersections(rectangles));

    try {
        assertEquals((int) inters.size(), 11);

        assertTrue(inters.find({ 0 }) != inters.end());
        assertEquals(inters.find({ 0 })->second, { 100, 100, 250, 80 });

        assertTrue(inters.find({ 1 }) != inters.end());
        assertEquals(inters.find({ 1 })->second, { 120, 200, 250, 150 });

        assertTrue(inters.find({ 2 }) != inters.end());
        assertEquals(inters.find({ 2 })->second, { 140, 160, 250, 100 });

        assertTrue(inters.find({ 3 }) != inters.end());
        assertEquals(inters.find({ 3 })->second, { 160, 140, 350, 190 });

        assertTrue(inters.find({ 0, 2 }) != inters.end());
        assertEquals(inters.find({ 0, 2 })->second, { 140, 160, 210, 20 });

        assertTrue(inters.find({ 0, 3 }) != inters.end());
        assertEquals(inters.find({ 0, 3 })->second, { 160, 140, 190, 40 });

        assertTrue(inters.find({ 1, 2 }) != inters.end());
        assertEquals(inters.find({ 1, 2 })->second, { 140, 200, 230, 60 });

        assertTrue(inters.find({ 1, 3 }) != inters.end());
        assertEquals(inters.find({ 1, 3 })->second, { 160, 200, 210, 130});

        assertTrue(inters.find({ 2, 3 }) != inters.end());
        assertEquals(inters.find({ 2, 3 })->second, { 160, 160, 230, 100 });

        assertTrue(inters.find({ 0, 2, 3 }) != inters.end());
        assertEquals(inters.find({ 0, 2, 3 })->second, { 160, 160, 190, 20 });

        assertTrue(inters.find({ 1, 2, 3 }) != inters.end());
        assertEquals(inters.find({ 1, 2, 3 })->second, { 160, 200, 210, 60 });
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

bool computeIntersectionsDifferentRects2() {
    std::vector<rectangles::Rectangle> rectangles;
    rectangles.push_back({ -7000, -8000, 250, 150 });
    rectangles.push_back({ 100, 100, 100, 100 });
    rectangles.push_back({ 100, 100, 50, 50 });
    rectangles.push_back({ 0, 0, 2500, 1500 });
    rectangles.push_back({ 200, 100, 100, 100 });
    rectangles.push_back({ 150, 150, 50, 50 });
    auto inters = intersToMap(rectangles::computeIntersections(rectangles));

    try {
        assertEquals((int) inters.size(), 14);

        assertTrue(inters.find({ 0 }) != inters.end());
        assertEquals(inters.find({ 0 })->second, { -7000, -8000, 250, 150 });

        assertTrue(inters.find({ 1 }) != inters.end());
        assertEquals(inters.find({ 1 })->second, { 100, 100, 100, 100 });

        assertTrue(inters.find({ 2 }) != inters.end());
        assertEquals(inters.find({ 2 })->second, { 100, 100, 50, 50 });

        assertTrue(inters.find({ 3 }) != inters.end());
        assertEquals(inters.find({ 3 })->second, { 0, 0, 2500, 1500 });

        assertTrue(inters.find({ 4 }) != inters.end());
        assertEquals(inters.find({ 4 })->second, { 200, 100, 100, 100 });

        assertTrue(inters.find({ 5 }) != inters.end());
        assertEquals(inters.find({ 5 })->second, { 150, 150, 50, 50 });

        assertTrue(inters.find({ 1, 2 }) != inters.end());
        assertEquals(inters.find({ 1, 2 })->second, { 100, 100, 50, 50 });

        assertTrue(inters.find({ 1, 3 }) != inters.end());
        assertEquals(inters.find({ 1, 3 })->second, { 100, 100, 100, 100 });

        assertTrue(inters.find({ 1, 5 }) != inters.end());
        assertEquals(inters.find({ 1, 5 })->second, { 150, 150, 50, 50 });

        assertTrue(inters.find({ 2, 3 }) != inters.end());
        assertEquals(inters.find({ 2, 3 })->second, { 100, 100, 50, 50 });

        assertTrue(inters.find({ 3, 4 }) != inters.end());
        assertEquals(inters.find({ 3, 4 })->second, { 200, 100, 100, 100 });

        assertTrue(inters.find({ 3, 5 }) != inters.end());
        assertEquals(inters.find({ 3, 5 })->second, { 150, 150, 50, 50 });

        assertTrue(inters.find({ 1, 2, 3 }) != inters.end());
        assertEquals(inters.find({ 1, 2, 3 })->second, { 100, 100, 50, 50 });

        assertTrue(inters.find({ 1, 3, 5 }) != inters.end());
        assertEquals(inters.find({ 1, 3, 5 })->second, { 150, 150, 50, 50 });
    } catch (AssertionError& e) {
        return false;
    }
    return true;
}

int main(int argc, char* argv[]) {
    std::map<std::string, Testcase> testcases;

    testcases["failToCreateRectangleWithNegativeWidth"] = &failToCreateRectangleWithNegativeWidth;
    testcases["failToCreateRectangleWithNegativeHeight"] = &failToCreateRectangleWithNegativeHeight;
    testcases["failToCreateRectangleDueToWidthOverflow"] = &failToCreateRectangleDueToWidthOverflow;
    testcases["failToCreateRectangleDueToHeightOverflow"] = &failToCreateRectangleDueToHeightOverflow;
    testcases["createRectanglesOfDifferentSizes"] = &createRectanglesOfDifferentSizes;
    testcases["checkIfRectangleIsEmpty"] = &checkIfRectangleIsEmpty;
    testcases["intersectionReflexive"] = &intersectionReflexive;
    testcases["intersectionSymmetric"] = &intersectionSymmetric;
    testcases["intersectionResults"] = &intersectionResults;
    testcases["computeIntersectionsEmptyList"] = &computeIntersectionsEmptyList;
    testcases["computeIntersectionsTheSameRect"] = &computeIntersectionsTheSameRect;
    testcases["computeIntersectionsDifferentRects1"] = &computeIntersectionsDifferentRects1;
    testcases["computeIntersectionsDifferentRects2"] = &computeIntersectionsDifferentRects2;

    runTest(testcases);
}
