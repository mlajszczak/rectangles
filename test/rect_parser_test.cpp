#include <map>
#include <iostream>
#include "../rect_parser.h"
#include "testing_utils.h"

bool parseNoRectsJson() {
    try {
        std::ifstream ifs("test/json/no_rects.json");
        auto rects = rectangles::parseRectangles(ifs);

        assertTrue(rects.empty());
    } catch (rectangles::ParsingError& e) {
        return false;
    } catch (AssertionError& e) {
        return false;
    }

    return true;
}

bool parseSingleRectJson() {
    try {
        std::ifstream ifs("test/json/single_rect.json");
        auto rects = rectangles::parseRectangles(ifs);

        assertEquals((int) rects.size(), 1);
        assertEquals(rects[0], {160, 140, 350, 190});
    } catch (rectangles::ParsingError& e) {
        return false;
    } catch (AssertionError& e) {
        return false;
    }

    return true;
}

bool parseManyRectsJson() {
    try {
        std::ifstream ifs("test/json/many_rects.json");
        auto rects = rectangles::parseRectangles(ifs);

        assertEquals((int) rects.size(), 4);
        assertEquals(rects[0], {-300, 100, 250, 80});
        assertEquals(rects[1], {-120, 200, 250, 150});
        assertEquals(rects[2], {140, 160, 250, 100});
        assertEquals(rects[3], {160, 140, 350, 190});
    } catch (rectangles::ParsingError& e) {
        return false;
    } catch (AssertionError& e) {
        return false;
    }

    return true;
}

bool failParsingEmptyObjectJson() {
    try {
        std::ifstream ifs("test/json/empty_object.json");
        auto rects = rectangles::parseRectangles(ifs);
    } catch (rectangles::ParsingError& e) {
        return true;
    } catch (...) {
        return false;
    }
    return false;
}

bool failParsingEmptyArrayJson() {
    try {
        std::ifstream ifs("test/json/empty_array.json");
        auto rects = rectangles::parseRectangles(ifs);
    } catch (rectangles::ParsingError& e) {
        return true;
    } catch (...) {
        return false;
    }
    return false;
}

bool failParsingBadPropertyNameJson() {
    try {
        std::ifstream ifs("test/json/bad_property_name.json");
        auto rects = rectangles::parseRectangles(ifs);
    } catch (rectangles::ParsingError& e) {
        return true;
    }
    return false;
}

bool failParsingJsonWithBadRectFormat1() {
    try {
        std::ifstream ifs("test/json/bad_rect_format1.json");
        auto rects = rectangles::parseRectangles(ifs);
    } catch (rectangles::ParsingError& e) {
        return true;
    }
    return false;
}

bool failParsingJsonWithBadRectFormat2() {
    try {
        std::ifstream ifs("test/json/bad_rect_format2.json");
        auto rects = rectangles::parseRectangles(ifs);
    } catch (rectangles::ParsingError& e) {
        return true;
    }
    return false;
}

bool failParsingJsonWithBadRectFormat3() {
    try {
        std::ifstream ifs("test/json/bad_rect_format3.json");
        auto rects = rectangles::parseRectangles(ifs);
    } catch (rectangles::ParsingError& e) {
        return true;
    }
    return false;
}

int main(int argc, char* argv[]) {
    std::map<std::string, Testcase> testcases;

    testcases["parseNoRectsJson"] = &parseNoRectsJson;
    testcases["parseSingleRectJson"] = &parseSingleRectJson;
    testcases["parseManyRectsJson"] = &parseManyRectsJson;
    testcases["failParsingEmptyObjectJson"] = &failParsingEmptyObjectJson;
    testcases["failParsingEmptyArrayJson"] = &failParsingEmptyArrayJson;
    testcases["failParsingBadPropertyNameJson"] = &failParsingBadPropertyNameJson;
    testcases["failParsingJsonWithBadRectFormat1"] = &failParsingJsonWithBadRectFormat1;
    testcases["failParsingJsonWithBadRectFormat2"] = &failParsingJsonWithBadRectFormat2;
    testcases["failParsingJsonWithBadRectFormat3"] = &failParsingJsonWithBadRectFormat3;
    testcases["failParsingJsonWithBadRectFormat4"] = &failParsingJsonWithBadRectFormat3;

    runTest(testcases);
}
