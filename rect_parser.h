#ifndef RECT_PARSER_H
#define RECT_PARSER_H

#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <vector>

#include "json.hpp"
#include "rectangles.h"

namespace rectangles {

    struct ParsingError: public std::exception {
        ParsingError(const std::string& message): message(message) {}

        virtual const char* what() const noexcept {
            return message.data();
        }

        virtual ~ParsingError() noexcept {}

        private:
            std::string message;
    };

    std::vector<rectangles::Rectangle> parseRectangles(std::istream& input) {
        nlohmann::json rects_json;

        try {
            input >> rects_json;
        } catch (std::logic_error& e) {
            throw ParsingError("error when parsing json");
        }

        if (!rects_json.is_object()) {
            throw ParsingError("invalid json");
        }

        if (rects_json.size() >  1) {
            throw ParsingError("invalid json");
        }

        if (rects_json.find("rects") == rects_json.end()) {
            throw ParsingError("invalid json");
        }

        if (!rects_json["rects"].is_array()) {
            throw ParsingError("invalid json");
        }

        std::vector<rectangles::Rectangle> rects;
        rects.resize(rects_json["rects"].size());

        try {
            std::transform(
                rects_json["rects"].cbegin(),
                rects_json["rects"].cend(),
                rects.begin(),
                [] (const auto& rect_json) {
                    if (!rect_json.is_object()) {
                        throw ParsingError("invalid json");
                    }
                    if (rect_json.find("x") == rect_json.end()) {
                        throw ParsingError("invalid json");
                    }
                    if (rect_json.find("y") == rect_json.end()) {
                        throw ParsingError("invalid json");
                    }
                    if (rect_json.find("w") == rect_json.end()) {
                        throw ParsingError("invalid json");
                    }
                    if (rect_json.find("h") == rect_json.end()) {
                        throw ParsingError("invalid json");
                    }
                    if (rect_json.size() >  4) {
                        throw ParsingError("invalid json");
                    }
                    if (!rect_json["x"].is_number()) {
                        throw ParsingError("invalid json");
                    }
                    if (!rect_json["y"].is_number()) {
                        throw ParsingError("invalid json");
                    }
                    if (!rect_json["w"].is_number()) {
                        throw ParsingError("invalid json");
                    }
                    if (!rect_json["h"].is_number()) {
                        throw ParsingError("invalid json");
                    }
                    return rectangles::Rectangle(rect_json["x"], rect_json["y"], rect_json["w"], rect_json["h"]);
                }
            );
        } catch (std::logic_error& e) {
            throw ParsingError("invalid json");
        }

        return rects;
    }

}

#endif
