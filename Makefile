all: main test

clean:
	rm -rf main rectangles_test  rect_parser_test *.o

rectangles.o:
	$(CXX) -std=c++14 -Wall -c rectangles.cpp

main: rectangles.o
	$(CXX) -std=c++14 rectangles.o -Wall main.cpp -o main

rectangles_test: rectangles.o
	$(CXX) -std=c++14 -Wall rectangles.o test/rectangles_test.cpp -o rectangles_test

rect_parser_test: rectangles.o
	$(CXX) -std=c++14 -Wall rectangles.o test/rect_parser_test.cpp -o rect_parser_test

build_test: rectangles_test rect_parser_test

test: build_test
	@echo "Running rectangles_test:"
	./rectangles_test
	@echo "Running rect_parser_test:"
	./rect_parser_test
