#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <vector>

#include "rectangles.h"
#include "rect_parser.h"

void printRectangles(const std::vector<rectangles::Rectangle>& rects) {
    for (unsigned i = 0; i < rects.size(); ++i) {
        std::cout << "\t" << i + 1 << ": Rectangle at " << rects[i].toString() << ".\n";
    }
}

void printIntersection(const rectangles::Intersection& intersection) {
    std::cout << "\tBetween rectangle ";

    int rectsCount = intersection.first.size();
    for (int i = 0; i < rectsCount; ++i) {
        if (i == 0) {
            std::cout << intersection.first[i] + 1;
        } else if (i == rectsCount - 1) {
            std::cout << " and " << intersection.first[i] + 1;
        } else {
            std::cout << ", " << intersection.first[i] + 1;
        }
    }
    std::cout << " at " << intersection.second.toString() << ".\n";
}

const int MAX_RECTANGLE_COUNT = 1000;

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cerr << "incorrect number of arumgents\n";
        return 1;
    }

    std::vector<rectangles::Rectangle> rects;
    std::vector<rectangles::Intersection> intersections;

    std::ifstream ifs;
    ifs.exceptions(std::ifstream::badbit);

    ifs.open(argv[1]);
    if (!ifs) {
        std::cerr << "could not open file\n";
        return 1;
    }

    try {
        rects = rectangles::parseRectangles(ifs);
        rects.resize(std::min((int) rects.size(), MAX_RECTANGLE_COUNT));
        intersections = rectangles::computeIntersections(rects);
    } catch(rectangles::ParsingError& e) {
        std::cerr << e.what() << "\n";
        return 1;
    } catch(std::system_error& e) {
        std::cerr << "could not read file\n";
        return 1;
    }

    std::sort(
        intersections.begin(),
        intersections.end(),
        [] (const auto& i1, const auto& i2) {
            if (i1.first.size() == i2.first.size()) {
                return i1.first < i2.first;
            } else {
                return i1.first.size() < i2.first.size();
            }
        });

    std::cout << "Input:\n";
    printRectangles(rects);

    std::cout << "\n";
    std::cout << "Intersections:\n";

    for (const auto& intersection: intersections) {
        if (intersection.first.size() > 1) {
            printIntersection(intersection);
        }
    }

    return 0;
}
