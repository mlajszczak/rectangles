#ifndef RECTANGLES_H
#define RECTANGLES_H

#include <stdexcept>
#include <utility>
#include <vector>

namespace rectangles {

    /**
     * Represents a rectangle.
     *
     * (x, y) - top left corner of the rectangle; w - width, h - height.
     *
     * The invariants hold:
     * - w >= 0 and h >= 0.
     * - neither (x + w) nor (y + h) overflow.
     *
     * A rectangle is empty iff h == 0 or w == 0.
     */
    class Rectangle {
        public:
            Rectangle(int x, int y, int w, int h): x(x), y(y), w(w), h(h) {
                if ((w < 0) || (h < 0)) {
                    throw std::invalid_argument("neither width nor height of a rectangle can be negative");
                }
                if (((x + w) < 0 && x > 0) || (((y + h) < 0) && y > 0)) {
                    throw std::invalid_argument("overflow when computing coordinates of vertices of rectangular");
                }
            }

            Rectangle() noexcept: x(0), y(0), w(0), h(0) {}

            int getX() const noexcept { return x; }
            int getY() const noexcept { return y; }
            int getW() const noexcept { return w; }
            int getH() const noexcept { return h; }

            std::string toString() const;

            bool operator==(const Rectangle& rhs) const noexcept;
            bool operator!=(const Rectangle& rhs) const noexcept;

            bool empty() const noexcept {
                return w == 0 || h == 0;
            }

            Rectangle intersect(const Rectangle& rect) const noexcept;

        private:
        int x, y, w, h;
    };

    /*
     * Intersection is a pair (list of identifiers of intersected rectangles, intersection rectangle)
     */
    using Intersection = std::pair<std::vector<int>, Rectangle>;

    /*
     * Computes a vector of intersections.
     *
     * Rectangles are identified by their indices in the input vector. For each non-empty subset
     * of indices there might be at most one intersection in the result. The list contains only
     * non-empty intersections (i.e. such that intersection rectangle is non-empty).
     *
     * Note that for all input rectangles the output vector contains exactly one intersection
     * that corresponds to this rectangle (the list of identifiers of intersected rectangles
     * is a singleton in this case).
     *
     * Resulting vector is not ordered in any particular way. However, lists of identifiers are
     * ordered (in ascending order).
     */
    std::vector<Intersection> computeIntersections(const std::vector<Rectangle>& rects);

}

#endif
